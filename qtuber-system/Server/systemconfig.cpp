#include "systemconfig.h"

SystemConfig* SystemConfig::instance = nullptr;

SystemConfig::SystemConfig() {
    QString strJson;

    QFile *file=new QFile(":/system/systemconfig.json");
    if(file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        strJson = file->readAll();
        file->close();
    } else {
        qDebug() << "\033[31mFailed to open file: \"systemconfig.json\" \033[0m";
        return;
    }

    QJsonObject jsonObject = QJsonDocument::fromJson(strJson.toUtf8()).object();
    config.tcpIp = jsonObject["tcpIp"].toString();
    config.tcpPort = jsonObject["tcpPort"].toInt();
    config.udpIp = jsonObject["udpIp"].toString();
    config.udpPort = jsonObject["udpPort"].toInt();
    config.dbHostName = jsonObject["dbHostName"].toString();
    config.dbName = jsonObject["dbName"].toString();
    config.dbUserName = jsonObject["dbUserName"].toString();
    config.dbPassword = jsonObject["dbPassword"].toString();
    config.infoHeadSizeWithoutFilename = jsonObject["infoHeadSizeWithoutFilename"].toInt();
    config.emptyFilename = jsonObject["emptyFilename"].toInt();
    config.localDataPath = jsonObject["localDataPath"].toString();
    config.robotPath = jsonObject["robotPath"].toString();

    delete file;
}

SystemConfig *SystemConfig::getInstance() {
    if (instance == nullptr) {
        instance = new SystemConfig();
    }
    return instance;
}

const Config &SystemConfig::getConfig() {
    return config;
}
