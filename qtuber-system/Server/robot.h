#ifndef ROBOT_H
#define ROBOT_H

#include <QProcess>
#include <QDebug>
#include <QCoreApplication>
#include <QTextCodec>
#include <QThread>

#include "systemconfig.h"

class Robot : public QThread
{
    Q_OBJECT
public:
    Robot();

public slots:
    void run();
    bool findAnswer();
    void exec();
    void recvStandOut();
    QString answer();
    void setQuestion(QString strQuestion);
    QString strQuestion() const;

    void setStrQuestion(const QString &strQuestion);

signals:
    void reRobotMsg();

private:
    QProcess *m_pProc;
    QString m_strQuestion;
    QString question;
};

#endif // ROBOT_H
