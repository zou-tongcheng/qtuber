#ifndef OFFLINEHANDLER_H
#define OFFLINEHANDLER_H

#include "infohead.h"
#include "transmitter.h"

#include <QHostAddress>
#include <QObject>
#include <QList>
#include <QTimerEvent>


#define OFFLINE_HANDLER OfflineHandler::getInstance()


struct InfoTimerKeeper {
    int timerId;
    InfoHead *infoHead;
    QByteArray dataBytes;
    QDataStream *dataStream;
    qint8 dataType;
    QHostAddress senderIp;
    quint16 senderPort;
};

class OfflineHandler : public QObject {
    Q_OBJECT

protected:
    OfflineHandler();

public:
    // 禁用拷贝构造函数
    OfflineHandler(OfflineHandler &other) = delete;
    // 禁用 = 运算符
    void operator=(const OfflineHandler &) = delete;
    // 静态方法获取单例
    static OfflineHandler *getInstance();

    void startSlot();

public slots:
    void pingOnline(InfoHead *infoHead,
                    QByteArray dataBytes,
                    QDataStream *dataStream,
                    qint8 dataType,
                    QHostAddress senderIp,
                    quint16 senderPort);

public:
    void timerEvent(QTimerEvent *event) override;

public slots:
    void rePingOnline(InfoHead *infoHead,
                      QByteArray dataBytes,
                      QDataStream *dataStream,
                      qint8 dataType,
                      QHostAddress senderIp,
                      quint16 senderPort);

private:
    // 静态单例指针
    static OfflineHandler *instance;
    QList<InfoTimerKeeper> keeperList;
};

#endif // OFFLINEHANDLER_H
