#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
    TRANS->startServer();
    OFFLINE_HANDLER->startSlot();

    //test
//    QJsonDocument doc;
//    QJsonObject obj;
//    obj.insert("msg", "@robot hello");
//    doc.setObject(obj);
//    QByteArray bytes = doc.toJson();
//    InfoHead *infoHead = new InfoHead(bytes.size(), 0, ServerOperation::SendGroupTextMsg, ServerOperation::Success,
//                                      3, 1, "");
//    TRANS->getOperation()->exec(infoHead, bytes, QHostAddress("192.168.1.102"), 7777);
}

MainWindow::~MainWindow() {
    delete ui;
}

