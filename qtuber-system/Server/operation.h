#ifndef OPERATION_H
#define OPERATION_H

#include "infohead.h"

#include <QHostAddress>

class Operation{
public:
    Operation();
    virtual void exec(InfoHead *infoHead, QByteArray data, QHostAddress ip, quint16 port) = 0;
    enum Opcode {
        //5 系统
        PingOnline = 500,
        RePingOnline = 5001,

        //1 个人模块
        SignIn = 100,
        ReSignIn = 1001,

        SignUpAvator = 101,
        ReSignUpAvator = 1011,

        SignUpInfo = 102,
        ReSignUpInfo = 1021,

        ModifyPassword = 103,
        ReModifyPassword = 1031,

        AlterPersonalInfo = 104,
        ReAlterPersonalInfo = 1041,

        //2 消息模块
        SendTextMsg = 200,
        ReSendTextMsg = 2001,
        OfflineSendTextMsg = 2002,

        SendImgMsg = 201,
        ReSendImgMsg = 2011,
        OfflineSendImgMsg = 2012,

        SendVoiceMsg = 202,
        ReSendVoiceMsg = 2021,
        OfflineSendVoiceMsg = 2022,

        SendGroupTextMsg = 203,
        ReSendGroupTextMsg = 2031,

        SendFileMsg = 204,
        ReSendFileMsg = 2041,

        //3 联系人模块
        GetNewFriend = 300,
        ReGetNewFriend = 3001,

        AddNewFriend = 301,
        ReAddNewFriend = 3011,
        AgreeNewFriend = 3012,
        ReAgreeNewFriend = 3013,

        GetFriendList = 302,
        ReGetFriendList = 3021,

        GetFriendInfo = 303,
        ReGetFriendInfo = 3031,

        RemoveFriend = 304,
        ReRemoveFriend = 3041,

        //4 群组模块
        GetNewGroup = 400,
        ReGetNewGroup = 4001,

        AddNewGroup = 401,
        ReAddNewGroup = 4001,

        GetGroupList = 402,
        ReGetGroupList = 4021,

        GetGroupInfo = 403,
        ReGetGroupInfo = 4031,

        CreateGroup = 404,
        ReCreateGroup = 4041,

        setGroupPermission = 405,
        ReSetGroupPermission = 4051,

        RemoveGroup = 406,
        ReRemoveGroup = 4061,

        AlterGroupInfo = 407,
        ReAlterGroupInfo = 4071,

    };
    enum Statecode {
        Success = 200,
        Fail = 400,
    };
};

#endif // OPERATION_H
