#include "transmitter.h"

Transmitter *Transmitter::instance = nullptr;
QSqlDatabase Transmitter::database;

Transmitter::Transmitter(Operation *operation, OperationThread *operationThread) {
    this->operation = operation;
}

Transmitter *Transmitter::getInstance() {
    if (instance == nullptr) {
        ServerOperation *operation = new ServerOperation();
        ServerOperationThread *operationThread = new ServerOperationThread();
        instance = new Transmitter(operation, operationThread);

        database = QSqlDatabase::addDatabase("QMYSQL");
        database.setHostName(SYSCONF.dbHostName);
        database.setDatabaseName(SYSCONF.dbName);
        database.setUserName(SYSCONF.dbUserName);
        database.setPassword(SYSCONF.dbPassword);
    }
    return instance;
}

QSqlDatabase Transmitter::getDatabase() {
    return getInstance()->database;
}

void Transmitter::newTcpConnection() {
    SocketContainer *container = new SocketContainer();
    QTcpSocket *socket = server->nextPendingConnection();

    connect(socket, SIGNAL(readyRead()), this, SLOT(readTcp()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnect()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));

    container->setSocket(socket);
    list.append(container);
}

void Transmitter::readUdp() {
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray bytes;
        QHostAddress senderIp;
        quint16 senderPort;
        bytes.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(bytes.data(), bytes.size(),
                                &senderIp, &senderPort);

        qDebug() << "senderIp: " << senderIp;
        qDebug() << "senderPort: " << senderPort;

        QDataStream out(&bytes, QIODevice::ReadOnly);
        qint16 headSize;
        out >> headSize;
        qDebug() << "headSize = " << headSize;
        QByteArray infoBytes;
        infoBytes.resize(headSize);
        out.readRawData(infoBytes.data(), infoBytes.size());

        auto infoHead = InfoHead::generateInfo(infoBytes);

        QByteArray data;
        data.resize(infoHead->getFileSize());
        out.readRawData(data.data(), data.size());

        qDebug() << "\033[35m readUdp \033[0m" << ",\top =" << infoHead->getOperation();
        if (static_cast<Operation::Opcode>(infoHead->getOperation()) == Operation::RePingOnline) {
            qDebug() << "\033[35m emit rePingOnline \033[0m";
            emit rePingOnline(infoHead, data, new QDataStream(), static_cast<qint8>(0),
                              senderIp, senderPort);
        } else {
            qDebug() << "\033[35m emit pingOnline \033[0m";
            emit pingOnline(infoHead, data, new QDataStream(), static_cast<qint8>(0),
                            senderIp, senderPort);
        }
    }
}

void Transmitter::readTcp() {
    for (SocketContainer* container : list) {
        QTcpSocket *socket = container->getSocket();

        if (socket->bytesAvailable() <= 0) continue;

        QDataStream in(socket);
        if (!container->isInfoSizeReady() && socket->bytesAvailable() >= (int)sizeof (qint16)) {
            qint16 infoSize;
            in >> infoSize;
            container->setInfoSize(infoSize);
            container->infoSizeReady();
        }

        if (container->isInfoSizeReady() && !container->isInfoReady() &&
            socket->bytesAvailable() >= container->getInfoSize()) {
            uint infoSize = container->getInfoSize();

            QByteArray bytes;
            bytes.resize(infoSize);
            in.readRawData(bytes.data(), bytes.size());
            container->setInfoHead(InfoHead::generateInfo(bytes));
            container->infoReady();
        }

        if (container->isInfoReady() && socket->bytesAvailable() >= container->getInfoHead()->getFileSize()) {
            qDebug() << "\033[35m readTcp \033[0m";
            emit pingOnline(container->getInfoHead(), QByteArray(), &in, static_cast<qint8>(1),
                            socket->peerAddress(), 7777);
        }
    }
}

void Transmitter::disconnect() {
    int i = 0;
    for (SocketContainer* container : list) {
        QTcpSocket *socket = container->getSocket();
        if (socket->state() == QTcpSocket::UnconnectedState) {
            socket->deleteLater();
            list.removeAt(i);
        }
        i ++;
    }
}

void Transmitter::startServer() {
    server = new QTcpServer();
    udpSocket = new QUdpSocket();
    udpSocket->bind(SYSCONF.udpPort, QUdpSocket::ShareAddress);
    testMsg = "";

    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readUdp()));
    connect(server, SIGNAL(newConnection()), this, SLOT(newTcpConnection()));

    QString host = SYSCONF.tcpIp;
    int port = SYSCONF.tcpPort;
    server->listen(QHostAddress(host), port);
}

void Transmitter::writeUdp(InfoHead *infoHead, QByteArray data, QHostAddress ip, qint16 port) {
    QByteArray bytes = infoHead->toBytes();
    bytes.append(data);
    udpSocket->writeDatagram(bytes.data(), bytes.size(),
                             ip, port);
}

void Transmitter::writeTcp(InfoHead *infoHead, QByteArray data, QHostAddress ip, qint16 port) {
    QByteArray bytes = infoHead->toBytes();
    bytes.append(data);
    QTcpSocket *tcpSocket = new QTcpSocket();
    tcpSocket->connectToHost(ip, port);
    tcpSocket->waitForConnected();
    tcpSocket->write(bytes);
    tcpSocket->waitForBytesWritten();
    tcpSocket->disconnect();
}

void Transmitter::error(QAbstractSocket::SocketError err) {
    qDebug() << err;
}

Operation *Transmitter::getOperation() const
{
    return operation;
}
