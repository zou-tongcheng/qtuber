#ifndef OPERATIONTHREAD_H
#define OPERATIONTHREAD_H

#include <QHostAddress>
#include <QThread>
#include "infohead.h"

class OperationThread : public QThread
{
public:
    OperationThread();
    virtual void run() = 0;
    virtual void setInfoHead(InfoHead *infoHead) = 0;
    virtual void setDataStream(QDataStream *dataStream) = 0;
    virtual void setIp(const QHostAddress &value) = 0;
    virtual void setPort(const quint16 &value) = 0;
};

#endif // OPERATIONTHREAD_H
