#ifndef INFOHEAD_H
#define INFOHEAD_H

#include <QString>
#include <QIODevice>
#include <QDataStream>
#include "systemconfig.h"

class InfoHead
{
public:
    InfoHead();
    InfoHead(qint64 fileSize, qint8 protocol, qint16 operation, qint16 stateCode,
             qint32 src, qint32 des, QString fileName);
    QByteArray toBytes();
    static InfoHead* generateInfo(QByteArray bytes);
    qint64 getFileSize() const;
    qint8 getProtocol() const;
    qint16 getOperation() const;
    qint16 getStateCode() const;
    qint32 getSrc() const;
    qint32 getDes() const;
    QString getFileName() const;

    void setOperation(const qint16 &value);

private:
    qint64 fileSize;
    qint8 protocol;
    qint16 operation;
    qint16 stateCode;
    qint32 src;
    qint32 des;
    QString fileName;
};

#endif // INFOHEAD_H
