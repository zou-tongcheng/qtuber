#ifndef SOCKETCONTAINER_H
#define SOCKETCONTAINER_H

#include <QTcpSocket>
#include "infohead.h"

class SocketContainer
{
public:
    SocketContainer();
    SocketContainer(QTcpSocket *socket, InfoHead *info);

    QTcpSocket *getSocket() const;
    void setSocket(QTcpSocket *value);

    InfoHead *getInfoHead() const;
    void setInfoHead(InfoHead *value);
    bool isInfoSizeReady();
    bool isInfoReady();
    void infoSizeReady();
    void infoReady();
    qint16 getInfoSize() const;
    void setInfoSize(short value);

private:
    QTcpSocket *socket;
    InfoHead *infoHead;
    bool infoSizeMark;
    bool infoMark;
    qint16 infoSize;
};

#endif // SOCKETCONTAINER_H
