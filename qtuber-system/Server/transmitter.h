#ifndef TRANSMITTER_H
#define TRANSMITTER_H

#include <QObject>
#include <QTcpServer>
#include <QUdpSocket>
#include "socketcontainer.h"
#include "serveroperationthread.h"
#include "operation.h"
#include "serveroperation.h"
#include <QSqlDatabase>

#define TRANS Transmitter::getInstance()
#define DB Transmitter::getDatabase()

class Transmitter : public QObject
{
    Q_OBJECT
protected:
    Transmitter(Operation *operation, OperationThread *operationThread);

public:
    // 禁用拷贝构造函数
    Transmitter(Transmitter &other) = delete;
    // 禁用 = 运算符
    void operator=(const Transmitter &) = delete;
    // 静态方法获取单例
    static Transmitter *getInstance();
    // 静态方法获取数据库单例
    static QSqlDatabase getDatabase();

    void startServer();
    void writeUdp(InfoHead *infoHead, QByteArray data, QHostAddress ip, qint16 port);
    void writeTcp(InfoHead *infoHead, QByteArray data, QHostAddress ip, qint16 port);

    Operation *getOperation() const;

private:
    // 静态单例指针
    static Transmitter *instance;
    static QSqlDatabase database;

public slots:
    void newTcpConnection();
    void readUdp();
    void readTcp();
    void disconnect();
    void error(QAbstractSocket::SocketError err);

signals:
    void pingOnline(InfoHead *infoHead,
                    QByteArray dataBytes,
                    QDataStream *dataStream,
                    qint8 dataType,
                    QHostAddress senderIp,
                    quint16 senderPort);
    void rePingOnline(InfoHead *infoHead,
                      QByteArray dataBytes,
                      QDataStream *dataStream,
                      qint8 dataType,
                      QHostAddress senderIp,
                      quint16 senderPort);

private:
    QTcpServer *server;
    QUdpSocket *udpSocket;
    QList<SocketContainer* > list;
    QString testMsg;
    Operation *operation;
};

#endif // TRANSMITTER_H
