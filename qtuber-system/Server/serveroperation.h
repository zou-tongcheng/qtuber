#ifndef SERVEROPERATION_H
#define SERVEROPERATION_H

#include "operation.h"
#include "infohead.h"
#include "transmitter.h"
#include "systemconfig.h"
#include "robot.h"

#include <QByteArray>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QTimer>
#include <QSqlTableModel>
#include <QJsonArray>
#include <QEventLoop>

class ServerOperation : public QObject, public Operation {
    Q_OBJECT
public:
    ServerOperation();
    void init();
    void exec(InfoHead *infoHead, QByteArray data, QHostAddress ip, quint16 port);
    qint32 getCurrentUserId();

private:
    void rePingOnline(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void signIn(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void signUpInfo(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void modifyPassword(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void alterPersonalInfo(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void sendTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void sendGroupTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void getNewFriend(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void addNewFriend(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void agreeNewFriend(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void getFriendList(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void getFriendInfo(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void getNewGroup(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void addNewGroup(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void getGroupList(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void getGroupInfo(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void defaultReturn();

    QEventLoop *loop;
private slots:
    void exitLoop();
};

#endif // OPERATION_H


