#ifndef SYSTEMCONFIG_H
#define SYSTEMCONFIG_H

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

#define SYSCONF SystemConfig::getInstance()->getConfig()

struct Config {
    QString tcpIp;
    int tcpPort;
    QString udpIp;
    int udpPort;
    QString dbHostName;
    QString dbName;
    QString dbUserName;
    QString dbPassword;
    int infoHeadSizeWithoutFilename;
    int emptyFilename;
    QString localDataPath;
    QString robotPath;
};


class SystemConfig {
protected:
    SystemConfig();

public:
    // 禁用拷贝构造函数
    SystemConfig(Config &other) = delete;
    // 禁用 = 运算符
    void operator=(const Config &) = delete;
    // 静态方法获取单例
    static SystemConfig *getInstance();
    // const & 修饰防止修改配置
    const Config &getConfig();

private:
    // 静态单例指针
    static SystemConfig *instance;
    Config config;
};

#endif // SYSTEMCONFIG_H
