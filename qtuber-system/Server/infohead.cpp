#include "infohead.h"
#include <QDebug>


InfoHead::InfoHead() {

}

InfoHead::InfoHead(qint64 fileSize, qint8 protocol, qint16 operation, qint16 stateCode,
                   qint32 src, qint32 des, QString fileName) {
    this->fileSize = fileSize;
    this->protocol = protocol;
    this->operation = operation;
    this->stateCode = stateCode;
    this->src = src;
    this->des = des;
    this->fileName = fileName;
}

QByteArray InfoHead::toBytes() {
    QByteArray infoBytes;
    QDataStream out(&infoBytes, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_12);
    out << fileSize << protocol << operation << stateCode
        << src << des << fileName;

    qint16 headSize = infoBytes.size();
    QByteArray bytes;
    QDataStream in(&bytes, QIODevice::WriteOnly);
    in.setVersion(QDataStream::Qt_5_12);
    in << headSize;
    bytes.append(infoBytes);
    return bytes;
}

InfoHead* InfoHead::generateInfo(QByteArray bytes) {
    QDataStream in(&bytes, QIODevice::ReadOnly);
    in.setVersion(QDataStream::Qt_5_12);

    qint64 fileSize;
    qint8 protocol;
    qint16 operation;
    qint16 stateCode;
    qint32 src;
    qint32 des;
    QString fileName;
    qDebug() << bytes;
    in >> fileSize >> protocol >> operation >> stateCode >> src >> des >> fileName;
    qDebug() << fileName;
    InfoHead *infoHead = new InfoHead(fileSize, protocol, operation, stateCode,
                                      src, des, fileName);
    return infoHead;
}

qint64 InfoHead::getFileSize() const {
    return fileSize;
}

qint8 InfoHead::getProtocol() const {
    return protocol;
}

qint16 InfoHead::getOperation() const {
    return operation;
}

qint32 InfoHead::getSrc() const {
    return src;
}

QString InfoHead::getFileName() const {
    return fileName;
}

void InfoHead::setOperation(const qint16 &value)
{
    operation = value;
}

qint16 InfoHead::getStateCode() const {
    return stateCode;
}

qint32 InfoHead::getDes() const {
    return des;
}
