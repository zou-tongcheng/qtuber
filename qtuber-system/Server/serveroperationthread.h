#ifndef SERVEROPERATIONTHREAD_H
#define SERVEROPERATIONTHREAD_H

#include <QThread>
#include <QDataStream>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QFile>
#include <QDateTime>
#include <QString>

#include "infohead.h"
#include "operation.h"
#include "operationthread.h"
#include "transmitter.h"

class ServerOperationThread : public OperationThread {
public:
    ServerOperationThread();
    void run() override;
    void setInfoHead(InfoHead *infoHead) override;
    void setDataStream(QDataStream *dataStream) override;
    void setIp(const QHostAddress &ip) override;
    void setPort(const quint16 &port) override;

    qint32 getCurrentUserId();

private:
    void signUpAvator(QByteArray bytes);
    void sendImgMsg(QByteArray bytes);
    void sendVoiceMsg(QByteArray bytes);
    void sendFileMsg(QByteArray bytes);
    void offlineSendTextMsg(QByteArray bytes);
    void offlineSendImgMsg(QByteArray bytes);
    void offlineSendVoiceMsg(QByteArray bytes);
    void defaultReturn();

private:
    InfoHead *infoHead;
    QDataStream *dataStream;
    QHostAddress ip;
    quint16 port;
};

#endif // OPERATIONTHREAD_H
