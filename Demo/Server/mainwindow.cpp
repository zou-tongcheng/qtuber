#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "servermanager.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ServerManager *server = new ServerManager();
    server->startServer();
}

MainWindow::~MainWindow()
{
    delete ui;
}

