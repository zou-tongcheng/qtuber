#ifndef OPERATIONTHREAD_H
#define OPERATIONTHREAD_H

#include <QThread>
#include <QDataStream>
#include <QFile>
#include "infohead.h"

class OperationThread : public QThread
{
public:
    OperationThread(InfoHead *infoHead, QDataStream *dataStream);
    void run();

private:
    InfoHead *infohead;
    QDataStream *in;
};

#endif // OPERATIONTHREAD_H
