#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include <QObject>
#include <QTcpServer>
#include <QUdpSocket>
#include "socketcontainer.h"
#include "operationthread.h"

class ServerManager : public QObject
{
    Q_OBJECT
public:
    ServerManager();
    void startServer();

public slots:
    void newTcpConnection();
    void readUdp();
    void readTcp();
    void disconnect();
    void error(QAbstractSocket::SocketError err);

private:
    QTcpServer *server;
    QUdpSocket *udpSocket;
    QList<SocketContainer* > list;
    QString testMsg;
};

#endif // SERVERMANAGER_H
