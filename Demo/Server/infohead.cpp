#include "infohead.h"
#include <QDebug>


InfoHead::InfoHead() {

}

InfoHead::InfoHead(qint64 fileSize, qint8 protocol, qint16 operation, qint32 src, qint32 des, QString fileName) {
    this->fileSize = fileSize;
    this->protocol = protocol;
    this->operation = operation;
    this->src = src;
    this->des = des;
    this->fileName = fileName;
}

QByteArray InfoHead::toBytes() {
    QByteArray bytes;
    QDataStream out(&bytes, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_12);

    qint16 headSize;
    headSize = 19 + fileName.size();
    out << headSize << fileSize << protocol << operation << src << des << fileName;
    return bytes;
}

InfoHead* InfoHead::generateInfo(QByteArray bytes) {
    QDataStream in(&bytes, QIODevice::ReadOnly);
    in.setVersion(QDataStream::Qt_5_12);

    qint64 fileSize;
    qint8 protocol;
    qint16 operation;
    qint32 src;
    qint32 des;
    QString fileName;
    qDebug() << bytes;
    in >> fileSize >> protocol >> operation >> src >> des >> fileName;
    qDebug() << fileName;
    InfoHead *infoHead = new InfoHead(fileSize, protocol, operation, src, des, fileName);
    return infoHead;
}

qint64 InfoHead::getFileSize() const {
    return fileSize;
}

qint8 InfoHead::getProtocol() const {
    return protocol;
}

qint16 InfoHead::getOperation() const {
    return operation;
}

qint32 InfoHead::getSrc() const {
    return src;
}

QString InfoHead::getFileName() const {
    return fileName;
}

qint32 InfoHead::getDes() const {
    return des;
}
