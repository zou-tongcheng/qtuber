#include "operationthread.h"
#include <QDebug>

OperationThread::OperationThread(InfoHead *infoHead, QDataStream *in)
{
    this->infohead = infoHead;
    this->in = in;
}

void OperationThread::run() {
    QByteArray bytes;
    bytes.resize(infohead->getFileSize());
    in->readRawData(bytes.data(), bytes.size());
    QString fileName = infohead->getFileName();
    fileName = "D:/blog/" + fileName;
    QFile file(fileName);

    qDebug() << "new file loaded";

    file.open(QFile::WriteOnly);
    file.write(bytes);
    file.close();
}

