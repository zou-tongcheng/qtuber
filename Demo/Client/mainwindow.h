#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "infohead.h"
#include "transmitter.h"

#include <QMainWindow>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QFile>
#include <QFileInfo>
#include <iostream>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonObject>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow :  public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonSignup_clicked();
    void on_pushButtonSignin_clicked();
    void on_pushButtonSentTextMsg_clicked();
    void combine_info(InfoHead *infoHead,
                QJsonDocument doc, QHostAddress ip, quint16 port);
    void touiReSentTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void on_pushButtonSendImg();


private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
