#include "clientoperationthread.h"
#include "operation.h"

ClientOperationThread::ClientOperationThread() {

}

void ClientOperationThread::run() {
    QByteArray bytes;
    bytes.resize(infoHead->getFileSize());
    dataStream->readRawData(bytes.data(), bytes.size());

    // TODO: 有需要客户端接收文件时，改变这里
    QFile file("C:/Qtuber/"
               + QString::number(infoHead->getDes())
               + "/file_recv/"
               + infoHead->getFileName());
    file.open(QFile::WriteOnly);
    file.write(bytes);
    file.close();

    switch (static_cast<Operation::Opcode>(infoHead->getOperation())) {

        default:
            return defaultReturn();
    }
}

void ClientOperationThread::setInfoHead(InfoHead *infoHead) {
    this->infoHead = infoHead;
}

void ClientOperationThread::setDataStream(QDataStream *dataStream) {
    this->dataStream = dataStream;
}

void ClientOperationThread::setIp(const QHostAddress &ip) {
    this->ip = ip;
}

void ClientOperationThread::setPort(const quint16 &port) {
    this->port = port;
}

void ClientOperationThread::defaultReturn() {
    qDebug() << "\033[32mClientOperationThread::exec switch in default case \033[0m";
}
