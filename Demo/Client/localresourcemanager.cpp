#include "localresourcemanager.h"

#include <QFile>

LocalResourceManager *LocalResourceManager::instance = nullptr;

LocalResourceManager::LocalResourceManager() {

}

LocalResourceManager *LocalResourceManager::getInstance() {
    if (instance == nullptr) {
        instance = new LocalResourceManager();
    }
    return instance;
}

QJsonDocument LocalResourceManager::getHistory(qint32 myId, qint32 otherId, LocalResourceManager::MsgType type) {
    QFile *file;
    if (type == SoloMsg) {
        file = new QFile(QString("C:/Qtuber/%1/solomsg/%2.json").arg(myId, otherId));
    } else if (type == GroupMsg) {
        file = new QFile(QString("C:/Qtuber/%1/groupmsg/%2.json").arg(myId, otherId));
    } else {
        qDebug() << "\033[32mLocalResourceManager::getHistory MsgType is not SoloMsg or GroupMsg \033[0m";
        return QJsonDocument();
    }
    if (!file->open(QIODevice::ReadOnly)) {
        qDebug() << "\033[32mLocalResourceManager::getHistory open file failed \033[0m";
        return QJsonDocument();
    }
    return QJsonDocument::fromJson(file->readAll());
}
