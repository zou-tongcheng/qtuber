#include "clientoperation.h"


ClientOperation::ClientOperation() {

}

void ClientOperation::exec(InfoHead *infoHead, QByteArray data, QHostAddress ip, quint16 port) {
    QJsonDocument doc = QJsonDocument::fromJson(data);
    qDebug() << "dakuohaokaitou" << data;
    switch (static_cast<Opcode>(infoHead->getOperation())) {
        case Operation::ReSignIn:
            emit TRANS->getSignalGun()->reSignIn(infoHead, doc, ip, port);
            break;
        case Operation::ReSignUpAvator:
            emit TRANS->getSignalGun()->reSignUpAvator(infoHead, doc, ip, port);
            break;
        case Operation::ReSignUpInfo:
            emit TRANS->getSignalGun()->reSignUpInfo(infoHead, doc, ip, port);
            break;
        case Operation::ReModifyPassword:
            emit TRANS->getSignalGun()->reModifyPassword(infoHead, doc, ip, port);
            break;
        case Operation::ReSentTextMsg:
            emit TRANS->getSignalGun()->reSentTextMsg(infoHead, doc, ip, port);
            break;
        default:
            defaultReturn();
    }
}

void ClientOperation::defaultReturn() {
    qDebug() << "\033[32mClientOperation::exec switch in default case \033[0m";
}
