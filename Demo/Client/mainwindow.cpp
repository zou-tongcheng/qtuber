#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
    TRANS->startServer();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButtonSignup_clicked() {
    QFile *file = new QFile("C:/Users/HP/Desktop/tmp/pic.png");
    file->open(QIODevice::ReadOnly);

    // userId应该由前端维护一个值
    qint32 userId = 123;

    TRANS->writeTcp(new InfoHead(file->size(), 1, Operation::SignUpAvator,
                                 Operation::Success, userId, 0,
                                 QFileInfo(*file).fileName()),
                    file->readAll(),
                    QHostAddress("192.168.1.103"), 6666);

    connect(TRANS->getSignalGun(), SIGNAL(reSignUpAvator(InfoHead*,
            QJsonDocument, QHostAddress, quint16)),
            this, SLOT(combine_info(InfoHead* info, QJsonDocument doc, QHostAddress ip, quint16 port)));
}


void MainWindow::combine_info(InfoHead *infoHead,
                              QJsonDocument doc, QHostAddress ip, quint16 port){

    QString account = "Cory";
    QString password = "123456";
    QString name = "Cory";
    QString gender = "男";
    qint32 age = 21;
    QString phone_number = "111111";
    QString email = "111@qq.com";
    QString signature = "111111";
    qint32 level = 1;

    QByteArray bytes;
    QJsonDocument *combine_doc = new QJsonDocument();
    QJsonObject obj;
    obj.insert("avator",doc.object()["path"]);
    obj.insert("account", account);
    obj.insert("name", name);
    obj.insert("gender", gender);
    obj.insert("age", age);
    obj.insert("phone_number", phone_number);
    obj.insert("email", email);
    obj.insert("signature", signature);
    obj.insert("level", level);

    combine_doc->setObject(obj);
    QByteArray combine_data = combine_doc->toJson();


}


void MainWindow::on_pushButtonSignin_clicked() {
    QString account = "lazydog";
    QString password = "hello";
    QJsonObject *obj = new QJsonObject();
    obj->insert("account", account);
    obj->insert("password", password);

    QJsonDocument doc;
    doc.setObject(*obj);
    QByteArray bytes = doc.toJson();
    qint64 fileSize = bytes.size();

    InfoHead *infoHead = new InfoHead(fileSize, 0, Operation::SignIn, Operation::Success, 0, 0,
                                      QString(""));
    TRANS->writeUdp(infoHead, QByteArray(), QHostAddress("192.168.1.103"), 7777);
}

void MainWindow::on_pushButtonSentTextMsg_clicked() {
    // 前端要发的消息
    QString msg = "前端要发的消息";
    qint32 senderId = 3;
    qint32 receiverId = 4;

    QJsonDocument *doc = new QJsonDocument();
    QJsonObject obj;
    obj.insert("msg", msg);
    doc->setObject(obj);
    QByteArray bytes = doc->toJson();

    TRANS->writeUdp(new InfoHead(bytes.size(), 0, Operation::SentTextMsg,
                                 Operation::Success, senderId, receiverId,
                                 QString("nigenbenbuzaishengyang")),
                    bytes,
                    QHostAddress("192.168.1.103"), 7777);
    connect(TRANS->getSignalGun(), SIGNAL(reSentTextMsg(InfoHead*, QJsonDocument, QHostAddress, quint16)),
            this, SLOT(touiReSentTextMsg(InfoHead*, QJsonDocument, QHostAddress, quint16)));
}
void MainWindow::touiReSentTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port) {
    qDebug() << "我是" << infoHead->getDes() << "收到来自" << infoHead->getSrc()
             << "的消息, 内容是：" << doc.object()["msg"].toString();
}

void MainWindow::on_pushButtonSendImg() {
    QFile file("");
    QByteArray bytes;
    bytes = file.readAll();
    qint64 fileSize = bytes.size();
    qint32 senderId = 1;
    qint32 receiverId = 2;
    InfoHead *infoHead = new InfoHead(fileSize, 0, Operation::SendImg, Operation::Success,
                                      senderId, receiverId,
                                      QString(""));
    TRANS->writeTcp(infoHead, bytes, QHostAddress("192.168.1.103"), 6666);
}
