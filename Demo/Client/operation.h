#ifndef OPERATION_H
#define OPERATION_H

#include "infohead.h"

#include <QHostAddress>

class Operation {
public:
    Operation();
    virtual void exec(InfoHead *infoHead, QByteArray data, QHostAddress ip, quint16 port) = 0;
    enum Opcode {
        SignIn = 1000,
        ReSignIn = 10001,
        SignUpAvator = 1001,
        ReSignUpAvator = 10011,
        SignUpInfo = 1002,
        ReSignUpInfo = 10021,
        ModifyPassword = 1003,
        ReModifyPassword = 10031,
        SentTextMsg = 2000,
        ReSentTextMsg = 20001,
        SendImg = 2001,
    };
    enum Statecode {
        Success = 200,
        Fail = 400,
    };
};

#endif // OPERATION_H
