﻿#include "talkmainwindow.h"

#pragma execution_character_set("utf-8")

TalkMainWindow::TalkMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowIcon(QIcon(":/btnIcon/qtuber.png"));
    setWindowTitle("Hi, Qtuber");
    QWidget::setMinimumSize(1000,600);
    qtab = new QTabWidget(this);
    qtab->setTabPosition(QTabWidget::West);
    qtab->setStyleSheet("background: #FFFFFF;");
    qtab->setFixedSize(1000,600);

    //页面 1 的主界面
    solochat = new TabSoloChat();
    qtab->addTab(solochat,"好友聊天");

    //页面 2 的主界面
    groupchat = new TabGroupChat();
    qtab->addTab(groupchat,"群组聊天");


    //页面 3 的主界面
    qwidget3 = new QWidget(this);
    QHBoxLayout *lay3 = new QHBoxLayout();

    LinkWidget * linkWiget = new LinkWidget(this);
    QWidget::setStyleSheet((QString("background:transparent;")));
    lay3->addWidget(linkWiget);
    qwidget3->setLayout(lay3);
    QIcon icon3(":/C:/Users/lenovo/Desktop/cesu.png");
    qtab->addTab(qwidget3,icon3,"联系人");

    //页面 4 的主界面
    qwidget4 = new tabSettingManage();
    qtab->addTab(qwidget4,"设置管理");

}

//点击对话框 展示消息记录
void TalkMainWindow::labelPressInfo(){
    /*
    //需要 全局变量存储 talkAccountID 即对话人的ID
    //单人聊天记录保存在 C:/Qtuber/[myId]/solomsg/[otherId].json 中
    //从该文件中读取
    talkaccountId = 2001;
    //有了Id

    QFile *file;
    file = new QFile(QString("C:/Qtuber/%1/solomsg/%2.json").arg(accountId, talkaccountId));

    if (!file->open(QIODevice::ReadOnly)) {
        qDebug() << "\033[32mLocalResourceManager::getHistory open file failed \033[0m";
    }
    QJsonDocument doc = QJsonDocument::fromJson(file->readAll());

    historyArray = doc.object()["history"].toArray();

    this->lw->clear();
    RecentContactBox * mw =  (RecentContactBox*)sender();
    for(int i=0;i < mw->linkMessage.size(); ++i){
        qDebug()<<mw->linkMessage[i].msg;

        if(mw->linkMessage[i].roleId == 0){
            mw->linkMessage[i].iconPath = ":/C:/Users/lenovo/Desktop/text.png";
            ChatItem * chattemp = new ChatItem(mw->linkMessage[i].msg, mw->linkMessage[i].iconPath, mw->linkMessage[i].roleId,talkWidget1);
            QListWidgetItem * listItem = new QListWidgetItem(lw);
            listItem->setSizeHint(chattemp->size());
            lw->setItemWidget(listItem,chattemp);
        }
        else{
            mw->linkMessage[i].iconPath = ":/C:/Users/lenovo/Desktop/0.png";
            ChatItem * chattemp = new ChatItem(mw->linkMessage[i].msg, mw->linkMessage[i].iconPath, mw->linkMessage[i].roleId,talkWidget1);
            QListWidgetItem * listItem = new QListWidgetItem(lw);
            listItem->setSizeHint(chattemp->size());
            lw->setItemWidget(listItem,chattemp);
        }
    }

    qDebug()<<"press mylabel";
*/
}

TabSoloChat *TalkMainWindow::getSolochat() const
{
    return solochat;
}

TabGroupChat * TalkMainWindow::getGroupchat() const{
    return  groupchat;
}

TalkMainWindow::~TalkMainWindow()
{

}

