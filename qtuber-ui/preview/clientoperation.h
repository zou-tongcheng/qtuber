#ifndef CLIENTOPERATION_H
#define CLIENTOPERATION_H

#include "operation.h"
#include "transmitter.h"

#include <QDebug>
#include <QJsonDocument>


class ClientOperation : public Operation{
public:
    ClientOperation();
    void exec(InfoHead *infoHead, QByteArray data, QHostAddress ip, quint16 port) override;

private:
    void pingOnline(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void defaultReturn();
};

#endif // CLIENTOPERATION_H
