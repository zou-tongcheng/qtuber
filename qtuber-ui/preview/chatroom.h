﻿#ifndef CHATROOM_H
#define CHATROOM_H

#include <QWidget>
#include <QHBoxLayout>
#include <QListWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QFile>
#include <QFileDialog>
#include "QDateTime"
#include "QJsonArray"
#include "QJsonParseError"
#include "QMessageBox"

#include "infohead.h"
#include "operation.h"
#include "transmitter.h"
#include "chatitem.h"
#include "addjsontools.h"
#include "emojipanel.h"


class Chatroom : public QWidget {
    Q_OBJECT
public:
    Chatroom(Operation::Opcode opcode);
    void init();
    void clearMsg();
    void setReId(qint32 reId);
    void addNewWidget(QWidget * qWidget);

public slots:
    void sendImgMsg();
    void reSendImgMsg(InfoHead *infoHead, QByteArray bytes, QHostAddress ip, quint16 port);
    void sendTextMsg();
    void reSendTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void reSendGroupTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void chooseEmoji(QString emoji);
    void clickEmojiBtn();
    void sendFileMsg();
    void reSendFileMsg(InfoHead *infoHead, QByteArray bytes, QHostAddress ip, quint16 port);
    void sendVoiceMsg();

private:
    qint32 reId;
    QListWidget *chatDesk;
    QPushButton *voiceBtn;
    QPushButton *imgBtn;
    QPushButton *fileBtn;
    QTextEdit *textEdit;
    QPushButton *emojiBtn;
    QPushButton *sendBtn;
    Operation::Opcode opcode;
    AddJsonTools addJsonTools;
};

#endif // CHATROOM_H
