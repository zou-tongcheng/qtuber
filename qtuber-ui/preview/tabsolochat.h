﻿ #ifndef TABSOLOCHAT_H
#define TABSOLOCHAT_H

#include <QScrollArea>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "QMap"

#include "recentcontactbox.h"
#include "keysender.h"
#include "chatroom.h"
#include "chatitem.h"
#include "voicemsg.h"
#include "mychatitem.h"
class TabSoloChat : public QWidget {
    Q_OBJECT
public:
    TabSoloChat();
    void init();
    void setRecentContactList(QList<RecentContact> recentContactList);
    void initRecentContactList(QJsonDocument doc);
    void generateRecentContactBoxes();

public slots:
    void handleBoxClicked(qint32 id);

private:
    QScrollArea *scrollArea;
    QList<RecentContact> recentContactList;
    QVBoxLayout * scrollLayout;
    Chatroom *chatroom;
    QWidget *scrollContainer;

    QMap<qint32,RecentContactBox*> myRecentContactBoxes;
};

#endif // TABSOLOCHAT_H
