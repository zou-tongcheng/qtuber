#ifndef LOCALRESOURCEMANAGER_H
#define LOCALRESOURCEMANAGER_H

#include <QJsonDocument>
#include <QDebug>

#include "config.h"

#define LRM LocalResourceManager::getInstance()

class LocalResourceManager {
protected:
    LocalResourceManager();

public:
    // 禁用拷贝构造函数
    LocalResourceManager(LocalResourceManager &other) = delete;
    // 禁用 = 运算符
    void operator=(const LocalResourceManager &) = delete;
    // 静态方法获取单例
    static LocalResourceManager *getInstance();

    enum MsgType {
        SoloMsg = 1,
        GroupMsg = 2
    };

    QJsonDocument getHistory(qint32 myId, qint32 otherId, MsgType type);
//    QJsonDocument appendHistoryRecord(qint32 myId, qint32 otherId, MsgType type, QString data, );

private:
    // 静态单例指针
    static LocalResourceManager *instance;
};

#endif // LOCALRESOURCEMANAGER_H
