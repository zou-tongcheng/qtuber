﻿#ifndef VOICEMSG_H
#define VOICEMSG_H

#include <QWidget>
#include <QMediaPlayer>

namespace Ui {
class VoiceMsg;
}

class VoiceMsg : public QWidget
{
    Q_OBJECT

public:
    explicit VoiceMsg(QWidget *parent = nullptr);
    explicit VoiceMsg(QString absoluteAvatorPath, QString time,
                      QString absoluteVoicePath, QWidget *parent = nullptr);
    ~VoiceMsg();


private slots:
    void on_playBtn_clicked();
    void on_stopBtn_clicked();
    void positionChanged(qint64 position);
    void durationChanged(qint64 duration);
    void on_horizontalSlider_valueChanged(int value);

private:
    Ui::VoiceMsg *ui;
    QMediaPlayer *player;
    QString absoluteVoicePath;
    bool isPlaying = false;
    QString positionTime;
    QString durationTime;
};

#endif // VOICEMSG_H
