﻿#include "emojipanel.h"
#include "ui_emojipanel.h"

EmojiPanel::EmojiPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EmojiPanel)
{
    ui->setupUi(this);
}

EmojiPanel::~EmojiPanel()
{
    delete ui;
}

void EmojiPanel::on_pushButton_clicked() {
    emit chooseEmoji(tr("😀"));
    qDebug() << "😀";
    this->close();
}

void EmojiPanel::on_pushButton_2_clicked()
{
    emit chooseEmoji(tr("🤣"));
    this->close();
}

void EmojiPanel::on_pushButton_3_clicked()
{
    emit chooseEmoji(tr("😋"));
    this->close();

}

void EmojiPanel::on_pushButton_5_clicked()
{
    emit chooseEmoji(tr("😁"));
    this->close();
}


void EmojiPanel::on_pushButton_6_clicked()
{
    emit chooseEmoji(tr("😊"));
    this->close();
}


void EmojiPanel::on_pushButton_8_clicked()
{
    emit chooseEmoji(tr("🥰"));
    this->close();
}

void EmojiPanel::on_pushButton_12_clicked()
{
    emit chooseEmoji(tr("🤪"));
    this->close();
}

void EmojiPanel::on_pushButton_9_clicked()
{
    emit chooseEmoji(tr("😐"));
    this->close();
}

void EmojiPanel::on_pushButton_10_clicked()
{
    emit chooseEmoji(tr("🤕"));
    this->close();
}

void EmojiPanel::on_pushButton_4_clicked()
{
    emit chooseEmoji(tr("🥳"));
    this->close();
}

void EmojiPanel::on_pushButton_11_clicked()
{
    emit chooseEmoji(tr("😅"));
    this->close();
}

void EmojiPanel::on_pushButton_7_clicked()
{
    emit chooseEmoji(tr("😭"));
    this->close();
}


void EmojiPanel::on_pushButton_13_clicked()
{
    emit chooseEmoji(tr("😤"));
    this->close();
}

void EmojiPanel::on_pushButton_14_clicked()
{
    emit chooseEmoji(tr("🤡"));
    this->close();
}

void EmojiPanel::on_pushButton_15_clicked()
{
    emit chooseEmoji(tr("👿"));
    this->close();
}
