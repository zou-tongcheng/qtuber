﻿#include "linkwidget.h"
#pragma execution_character_set("utf-8")
LinkWidget::LinkWidget(QWidget *parent)
    : QMainWindow(parent)
{

    resize(350,800);

    loadFile();
    init_friends();
    init_group();
    this->setStyleSheet("QGroupBox{background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
                             "stop: 0 #ffffff, stop: 1 #845ef7);"
                             "border: 2px solid #845ef7;"
                             "border-radius: 5px;"
                             "margin-top: 20px;}"
                        "QMainWindow{background:#F5F5F5}"
                        "QPushButton{border-radius:10px;"
                        "background:transparent;}"
                        "QToolBox{background:#F5F5F5;}"
                        "QToolBox::tab {background-color:#845ef7;border-radius:8px; color:#ffffff;}");
    //准备好的抽屉插入TOOlBox中
    MyComboBox *mcbox_friends = new MyComboBox(this);
    MyComboBox *mcbox_group = new MyComboBox(this);

    mcbox_friends->resize(80,20);
    mcbox_group->resize(80,20);

    mcbox_friends->addItem("");
    mcbox_group->addItem("");
    mcbox_friends->setItemText(0,"好友");
    mcbox_group->setItemText(0,"群聊");

    mcbox_friends->move(50,30);
    mcbox_group->move(150,30);

    connect(mcbox_friends,&MyComboBox::clicked,this,&LinkWidget::change_friends);
    connect(mcbox_group,&MyComboBox::clicked,this,&LinkWidget::change_group);

    add = new QPushButton(this);
    add->setIcon(QIcon(":/btnIcon/addFriend.png"));
    add->resize(40,40);
    add->move(200,45);

    connect(add,&QPushButton::clicked,this,&LinkWidget::click_add);

    tabsetting = new FriendSetting(this);
    tabsetting->setStyleSheet("border-radius:10px");
    tabsetting->resize(660,520);
    tabsetting->move(250,30);
}
void LinkWidget::init_friends()
{
    QGroupBox *gb_friend = new QGroupBox;
    QVBoxLayout *vBoxLay_friend = new QVBoxLayout(gb_friend);

    vBoxLay_friend->setMargin(5);
    vBoxLay_friend->setAlignment(Qt::AlignHCenter);

    for(auto &peopleData:peopleDataList){
        initToolButton(&toolbtn_people,peopleData.nickName,peopleData.headPortrait);
        vBoxLay_friend->addWidget(toolbtn_people);

        idd = peopleData.id;

        toolButtonContianers.append(ToolButtonContianer(toolbtn_people, idd));
        connect(toolbtn_people,&QToolButton::clicked,this,[this](){click_friends();});

    }


   // lay1->addStretch();

    initToolButton(&toolbtn11,"刘翔","11.png");
    initToolButton(&toolbtn12,"詹姆斯","12.png");
    QGroupBox *gb_stranger = new QGroupBox;
    QVBoxLayout *vBoxLay_stranger = new QVBoxLayout(gb_stranger);
    vBoxLay_stranger->addWidget(toolbtn11);
    vBoxLay_stranger->addWidget(toolbtn12);
    vBoxLay_stranger->setMargin(5);
    vBoxLay_stranger->setAlignment(Qt::AlignHCenter);



    initToolButton(&toolbtn21,"惠子",":/Avatar/Avatar/nn.jpeg");

    QGroupBox *gb_blackList = new QGroupBox;
    QVBoxLayout *vBoxLay_blackList = new QVBoxLayout(gb_blackList);
    vBoxLay_blackList->addWidget(toolbtn21);

    vBoxLay_blackList->setMargin(5);
    vBoxLay_blackList->setAlignment(Qt::AlignHCenter);

    tbox_friends = new QToolBox(this);
    tbox_friends->resize(170,350);
    tbox_friends->move(50,80);
    tbox_friends->addItem(gb_friend,"我的好友");
    tbox_friends->addItem(gb_stranger,"陌生人");
    tbox_friends->addItem(gb_blackList,"黑名单");
    tbox_friends->hide();

}
void LinkWidget::init_group()
{
    initToolButton(&toolbtn_top,"一家亲",":/Avatar/Avatar/together.jpeg");



    QGroupBox *gb_top = new QGroupBox;
    QVBoxLayout *vBoxLay_top = new QVBoxLayout(gb_top);

    vBoxLay_top->setMargin(5);
    vBoxLay_top->setAlignment(Qt::AlignHCenter);

    vBoxLay_top->addWidget(toolbtn_top);


    for(auto &groupData:groupDataList){
        initToolButton(&toolbtn_group,groupData.nickName,groupData.headPortrait);
        vBoxLay_top->addWidget(toolbtn_group);
        idd = groupData.id;

        toolButtonContianers.append(ToolButtonContianer(toolbtn_group, idd));
        connect(toolbtn_group,&QToolButton::clicked,this,[this](){click_friends();});

    }

   // lay1->addStretch();

    initToolButton(&toolbtn_creat,"刘翔","11.png");

    QGroupBox *gb_creat = new QGroupBox;
    QVBoxLayout *vBoxLay_creat = new QVBoxLayout(gb_creat);
    vBoxLay_creat->addWidget(toolbtn_creat);

    vBoxLay_creat->setMargin(5);
    vBoxLay_creat->setAlignment(Qt::AlignHCenter);



    initToolButton(&toolbtn_manage,"惠子",":/Avatar/Avatar/together.jpeg");

    QGroupBox *gb_manage= new QGroupBox;
    QVBoxLayout *vBoxLay_manage = new QVBoxLayout(gb_manage);
    vBoxLay_manage->addWidget(toolbtn_manage);

    vBoxLay_manage->setMargin(5);
    vBoxLay_manage->setAlignment(Qt::AlignHCenter);

    tbox_group = new QToolBox(this);
    tbox_group->resize(170,350);
    tbox_group->move(50,80);
    tbox_group->addItem(gb_top,"置顶群");
    tbox_group->addItem(gb_creat,"我创建的群");
    tbox_group->addItem(gb_manage,"我管理的群");
    tbox_group->hide();

}

void LinkWidget::click_add(){
    //设置QPushButtuon 的三态， QLineEdit 的样式， QLable 和 整体的背景色
    QString style = "QPushButton{background-color:#6A5ACD; color:#ffffff; border-radius:10px; min-height:30px; min-width:60px; font:13px \"Microsoft YaHei\";}"
                    "QPushButton:hover{background:#FFF0F5;}"
                    "QPushButton:pressed{background:#FFF0F5;}"

                    "QLineEdit{padding:4px;padding-left:10px;border-radius:3px;color:#000000;font:13px \"Microsoft YaHei\";}"
                    "QLineEdit:focus{border:2px solid #6A5ACD;}"

                    "QLabel{color:#000000; font:12px \"Microsoft YaHei\"; font-weight:bolder;}"

                    "QInputDialog{background-color:#F5F5F5; }";

    QInputDialog dia;
    dia.setWindowIcon(QIcon(":/btnIcon/qtuber.png"));
    dia.resize(300,750);
    dia.setStyleSheet(style);
    dia.setWindowTitle("添加好友或群");
    dia.setLabelText("输入QQ号或群号：");
    dia.setInputMode(QInputDialog::TextInput);//可选参数：DoubleInput  TextInput

    if(dia.exec() == QInputDialog::Accepted)
    {
       qDebug() << dia.textValue();
    }

}

void LinkWidget::initToolButton(QToolButton **tb, QString name, QString pathpic)
{

    (*tb) = new QToolButton;
    (*tb)->setText(name);
    (*tb)->setIcon(QPixmap(pathpic));
    (*tb)->setIconSize(QSize(20,20));
    (*tb)->setAutoRaise(true);
    (*tb)->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

}

void LinkWidget::change_friends(){

    tbox_friends->show();
    tbox_group->hide();

}

void LinkWidget::change_group(){
    tbox_group->show();
    tbox_friends->hide();

}

void LinkWidget::loadFile(){
    QFile loadFile("F:\\QtDemo\\contact\\data.json");

     if(!loadFile.open(QIODevice::ReadOnly))
        {
            qDebug() << "could't open projects json";
            return;
        }
        QByteArray allData = loadFile.readAll();
       // qDebug()<<allData;
        loadFile.close();

        QJsonParseError json_error;

        QJsonDocument docu = QJsonDocument::fromJson(allData,&json_error);


        if(!docu.isNull()&&(json_error.error == QJsonParseError::NoError)){
            if(docu.isObject()){
                QJsonObject rootObj = docu.object();
               // qDebug()<<"docu.isObject";


                //输出所有key查看一下
                QStringList keys = rootObj.keys();
//                qDebug()<<"QStringList keys";
//                qDebug()<<keys;

                if(rootObj.contains("people"))
                {
                    qDebug()<<"rootObj.contains(people)";
                    QJsonValue value = rootObj.value("people");
                    if(value.isArray()){
                        QJsonArray subArray = value.toArray();
//                        qDebug()<<" subArray";
//                        qDebug()<<subArray;


                        for(int i = 0; i< subArray.size(); i++)
                        {
                            QJsonObject subObj = subArray.at(i).toObject();
                            QStringList fList = subObj.keys();
                            //qDebug()<<"subObj结果是"<<subObj;
                            //qDebug()<<"fList结果是"<<fList;
                            for(int j = 0; j < subObj.size(); j++)
                            {
                                str = fList.at(j);
                                //QJsonValue val = subObj.value(str);
                                //qDebug()<<val;
                                if(str=="id"){
                                    peopleData.id=subObj.value(str).toInt();
                                }else if(str=="nickName"){
                                    peopleData.nickName=subObj.value(str).toString();
                                }else {
                                    peopleData.headPortrait=subObj.value(str).toString();
                                }

                            }
                            //qDebug() << i<<" value is:" << subArray.at(i).toString();
                            peopleDataList.append(peopleData);
                            //qDebug()<<"peopleData.headPortrait"<<peopleData.headPortrait;
                        }

                        //qDebug()<<"dataList.at(0).nickName"<<peopleDataList.at(0).nickName;

                    }
                }
                if(rootObj.contains("group"))
                {

                    //qDebug()<<"rootObj.contains(group)";
                    QJsonValue value = rootObj.value("group");
                    if(value.isArray()){
                        QJsonArray subArray = value.toArray();
                       // qDebug()<<" subArray";
                       // qDebug()<<subArray.at(0);

                        for(int i = 0; i< subArray.size(); i++)
                        {
                            QJsonObject subObj = subArray.at(i).toObject();
                            QStringList fList = subObj.keys();
                            //qDebug()<<"subObj结果是"<<subObj;
                            //qDebug()<<"fList结果是"<<fList;
                            for(int j = 0; j < subObj.size(); j++)
                            {
                                str = fList.at(j);
                                if(str=="id"){
                                    groupData.id=subObj.value(str).toInt();
                                }else if(str=="nickName"){
                                    groupData.nickName=subObj.value(str).toString();
                                }else {
                                    groupData.headPortrait=subObj.value(str).toString();
                                }

                            }
                            //qDebug() << i<<" value is:" << subArray.at(i).toString();
                            groupDataList.append(groupData);
                        }

                    }
                }

            }
        }else {
            qDebug()<<json_error.errorString();
        }

}

int LinkWidget::click_friends(){
    QToolButton * btn = static_cast<QToolButton*>(sender());
    int id;
    for (auto each: toolButtonContianers) {
        if (each.btn == btn) {
            id = each.id;
        }
    }
    qDebug() << id;
    return id;

}

int LinkWidget::click_groups(){
    QToolButton * btn = static_cast<QToolButton*>(sender());
    int id;
    for (auto each: toolButtonContianers) {
        if (each.btn == btn) {
            id = each.id;
        }
    }
    qDebug() << id;
    return id;

}
LinkWidget::~LinkWidget()
{

}



