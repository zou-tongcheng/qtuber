﻿#include "chatitem.h"
#include "ui_chatitem.h"
#include "QDebug"
ChatItem::ChatItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChatItem)
{
    ui->setupUi(this);
    //允许消息自动换行
    ui->msg->setWordWrap(true);
    ui->avator->setStyleSheet("background:transparent;");
    ui->msg->setStyleSheet("background-color:#F5F5F5;"
                           "border-radius:10px;"
                           "color:#000000;");
}

ChatItem::~ChatItem()
{
    delete ui;
}

void ChatItem::setAvator(QString avatorPath){
    QPixmap pix;
    pix.load(avatorPath);
    pix = pix.scaled(ui->avator->size(), Qt::KeepAspectRatio);
    ui->avator->setPixmap(pix);
}

void ChatItem::setTime(QString time){
    ui->time->setText(time);
    ui->time->adjustSize();
    int height = ui->time->height();
    int width = ui->time->width();
    ui->time->resize(width + 20, height);
}

void ChatItem::setMsg(QString msg){
    ui->msg->setText(msg);
    ui->msg->setWordWrap(true);
    ui->msg->adjustSize();
    int height = ui->msg->height();
    int width = ui->msg->width();
    ui->msg->resize(width + 40, height+10);
}
void ChatItem::addPic(QString picPath){
    QPixmap pix;
    pix.load(picPath);
    pix = pix.scaled(ui->msg->size(), Qt::KeepAspectRatio);
    ui->msg->setPixmap(pix);
}
