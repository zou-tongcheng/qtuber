﻿#include "mycombobox.h"
#pragma execution_character_set("utf-8")
MyComboBox::MyComboBox(QWidget *parent):QComboBox(parent)
{
    this->setStyleSheet("background-color:#845ef7;"
                        "border-radius:8px;"
                        "padding:1px 18px 1px 3px;"
                        "color:#ffffff;"
                        "font-weight:bolder;");
}

MyComboBox::~MyComboBox()
{

}
void MyComboBox::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {

        emit clicked();  //触发clicked信号
    }

    QComboBox::mousePressEvent(event);  //将该事件传给父类处理，这句话很重要，如果没有，父类无法处理本来的点击事件
}
