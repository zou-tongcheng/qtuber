﻿#include "mychatitem.h"
#include "ui_mychatitem.h"


MyChatItem::MyChatItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyChatItem)
{
    ui->setupUi(this);
    //允许消息自动换行
    ui->msg->setWordWrap(true);
    ui->avator->setStyleSheet("background:transparent;");

    ui->msg->setStyleSheet("background-color:#8A2BE2;"
                           "border-radius:10px;"
                           "color:#ffffff;");
    ui->time->setStyleSheet("border-radius:10px;"
                            "background:transparent;"
                            "border-radius:5px;");
    ui->msg->setMaximumWidth(250);
}

MyChatItem::~MyChatItem()
{
    delete ui;
}

void MyChatItem::setAvator(QString avatorPath){
    QPixmap pix;
    pix.load(avatorPath);
    pix = pix.scaled(ui->avator->size(), Qt::KeepAspectRatio);
    ui->avator->setPixmap(pix);
}

void MyChatItem::setTime(QString time){
    ui->time->setText(time);
    ui->time->adjustSize();
    int height = ui->time->height();
    int width = ui->time->width();
    ui->time->resize(width+20,height);
}

void MyChatItem::setMsg(QString msg){
    ui->msg->setText(msg);
    ui->msg->setWordWrap(true);
    ui->msg->adjustSize();
    int height = ui->msg->height();
    int width = ui->msg->width();
    ui->msg->resize(width + 40, height + 10);
}

void MyChatItem::addPic(QString picPath){
    QPixmap pix;
    pix.load(picPath);
    pix = pix.scaled(ui->msg->size(), Qt::KeepAspectRatio);
    ui->msg->setPixmap(pix);
}
