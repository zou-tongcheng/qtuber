#include "socketcontainer.h"

SocketContainer::SocketContainer() {
    infoSizeMark = false;
    infoMark = false;
    infoSize = 0;
}

SocketContainer::SocketContainer(QTcpSocket *socket, InfoHead *infoHead) {
    this->socket = socket;
    this->infoHead = infoHead;
}

QTcpSocket *SocketContainer::getSocket() const {
    return socket;
}

void SocketContainer::setSocket(QTcpSocket *value) {
    socket = value;
}

InfoHead *SocketContainer::getInfoHead() const {
    return infoHead;
}

void SocketContainer::setInfoHead(InfoHead *value) {
    infoHead = value;
}

void SocketContainer::infoSizeReady() {
    infoSizeMark = true;
}

void SocketContainer::infoReady() {
    infoMark = true;
}

qint16 SocketContainer::getInfoSize() const {
    return infoSize;
}

void SocketContainer::setInfoSize(short value) {
    infoSize = value;
}

bool SocketContainer::isInfoSizeReady() {
    return infoSizeMark;
}

bool SocketContainer::isInfoReady() {
    return infoMark;
}
