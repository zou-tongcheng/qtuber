#include "dialogsuccess.h"
#include "ui_dialogsuccess.h"

DialogSuccess::DialogSuccess(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DialogSuccess)
{
    ui->setupUi(this);
    setWindowTitle("操作成功");
}

DialogSuccess::~DialogSuccess()
{
    delete ui;
}
