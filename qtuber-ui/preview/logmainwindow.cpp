﻿#include "logmainwindow.h"
#include "ui_logmainwindow.h"
#include <QPushButton>
#include <QMessageBox>
#include <QIcon>
#pragma execution_character_set("utf-8")

LogMainWindow::LogMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::LogMainWindow)
{
    ui->setupUi(this);
    setWindowTitle("登录注册界面");
    setWindowIcon(QIcon(":/C:/Users/lenovo/Desktop/log/log/image/1.jpg"));
    connect(ui->login,&QPushButton::clicked,[=](){
        //登录
        if((ui->user->text()=="lllaaa") && (ui->password->text()=="123")){
            QMessageBox *pmsg1 = new QMessageBox(QMessageBox::Information,"登陆成功","登陆成功",QMessageBox::Ok,this);
            pmsg1->exec();
        }else{
            QMessageBox *pmsg2 = new QMessageBox(QMessageBox::Critical,"登陆失败","账号密码错误",QMessageBox::Ok,this);
            pmsg2->exec();
        }
    });
}

LogMainWindow::~LogMainWindow()
{
    delete ui;
}


void LogMainWindow::on_reg_clicked()
{
    Register reg;
    reg.exec();
}
