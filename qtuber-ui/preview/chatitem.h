﻿#ifndef CHATITEM_H
#define CHATITEM_H

#include <QLabel>
#include <QWidget>

namespace Ui {
class ChatItem;
}

class ChatItem : public QWidget
{
    Q_OBJECT

public:
    explicit ChatItem(QWidget *parent = nullptr);
    ~ChatItem();
    void setAvator(QString avatorPath);
    void setTime(QString time);
    void setMsg(QString msg);
    void addPic(QString picPath);
private:
    Ui::ChatItem *ui;

};

#endif // CHATITEM_H
