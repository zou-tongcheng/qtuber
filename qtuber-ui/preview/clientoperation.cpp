﻿#include "clientoperation.h"


ClientOperation::ClientOperation() {

}

void ClientOperation::exec(InfoHead *infoHead, QByteArray data, QHostAddress ip, quint16 port) {
    QJsonDocument doc = QJsonDocument::fromJson(data);
    switch (static_cast<Opcode>(infoHead->getOperation())) {
        case Operation::PingOnline:
            pingOnline(infoHead, doc, ip, port);
            break;
        case Operation::ReSignIn:
            emit TRANS->getSignalGun()->reSignIn(infoHead, doc, ip, port);
            break;
        case Operation::ReSignUpAvator:
            emit SGUN->reSignUpAvator(infoHead, doc, ip, port);
            break;
        case Operation::ReSignUpInfo:
            emit SGUN->reSignUpInfo(infoHead, doc, ip, port);
            break;
        case Operation::ReModifyPassword:
            emit SGUN->reModifyPassword(infoHead, doc, ip, port);
            break;
        case Operation::ReSendTextMsg:
            emit SGUN->reSendTextMsg(infoHead, doc, ip, port);
            break;
        case Operation::ReSendImgMsg:
            emit SGUN->reSendImgMsg(infoHead, data, ip, port);
            break;
        case Operation::ReSendGroupTextMsg:
            emit SGUN->reSendGroupTextMsg(infoHead, doc, ip, port);
            break;
        default:
            defaultReturn();
    }
}

void ClientOperation::pingOnline(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port) {
    TRANS->writeUdp(new InfoHead(0,
                                 0,
                                 Operation::RePingOnline,
                                 Operation::Success,
                                 infoHead->getDes(),
                                 infoHead->getSrc(),
                                 QString("")),
                    QByteArray(),
                    CONF.serverIp,
                    CONF.udpPort);
    // TODO: delete following lines
    qDebug() << "des" << infoHead->getDes();
    qDebug() << "src" << infoHead->getSrc();
    qDebug() << QString("[pingOnline] CONF.serverIp = %1, ip = %2, CONF.udpPort = %3, port = %4")
                .arg(CONF.serverIp.toString(), ip.toString(), QString::number(CONF.udpPort), QString::number(port));
}

void ClientOperation::defaultReturn() {
    qDebug() << "\033[32mClientOperation::exec switch in default case \033[0m";
}
