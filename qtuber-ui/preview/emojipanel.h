﻿#ifndef EMOJIPANEL_H
#define EMOJIPANEL_H

#include <QWidget>
#include "QDebug"
namespace Ui {
class EmojiPanel;
}

class EmojiPanel : public QWidget
{
    Q_OBJECT

public:
    explicit EmojiPanel(QWidget *parent = nullptr);
    ~EmojiPanel();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_13_clicked();

    void on_pushButton_14_clicked();

    void on_pushButton_15_clicked();

signals:
    void chooseEmoji(QString emoji);

private:
    Ui::EmojiPanel *ui;
};

#endif // EMOJIPANEL_H
