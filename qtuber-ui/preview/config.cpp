﻿#include "config.h"


Config* Config::instance = nullptr;

Config::Config() {
    QString strJson;

    QFile *file=new QFile(":/config/config.json");
    if(file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        strJson = file->readAll();
        file->close();
    } else {
        qDebug() << "\033[31mFailed to open file: \"systemconfig.json\" \033[0m";
        return;
    }

    QJsonObject jsonObject = QJsonDocument::fromJson(strJson.toUtf8()).object();
    config.serverIp = QHostAddress(jsonObject["serverIp"].toString());
    config.localIp = QHostAddress(jsonObject["localIp"].toString());
    config.tcpPort = jsonObject["tcpPort"].toInt();
    config.udpPort = jsonObject["udpPort"].toInt();
    config.localDataPath = jsonObject["localDataPath"].toString();
    config.userId = 0;

    delete file;
}

Config *Config::getInstance() {
    if (instance == nullptr) {
        instance = new Config();
    }
    return instance;
}

const ConfigAttr &Config::getConfig() {
    return config;
}

void Config::setUserId(qint32 value) {
    config.userId = value;
}
