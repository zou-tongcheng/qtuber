#ifndef TRANSMITTER_H
#define TRANSMITTER_H

#include <QObject>
#include <QTcpServer>
#include <QUdpSocket>
#include "socketcontainer.h"
#include "clientoperationthread.h"
#include "operation.h"
#include "clientoperation.h"
#include "signalgun.h"
#include "config.h"

#define TRANS Transmitter::getInstance()
#define SGUN Transmitter::getInstance()->getSignalGun()

class Transmitter : public QObject
{
    Q_OBJECT
protected:
    Transmitter(Operation *operation, OperationThread *operationThread);

public:
    // 禁用拷贝构造函数
    Transmitter(Transmitter &other) = delete;
    // 禁用 = 运算符
    void operator=(const Transmitter &) = delete;
    // 静态方法获取单例
    static Transmitter *getInstance();

    void startServer();
    void writeUdp(InfoHead *infoHead, QByteArray data, QHostAddress ip, qint16 port);
    void writeTcp(InfoHead *infoHead, QByteArray data, QHostAddress ip, qint16 port);

    SignalGun *getSignalGun() const;
    Operation *getOperation() const;

private:
    // 静态单例指针
    static Transmitter *instance;

public slots:
    void newTcpConnection();
    void readUdp();
    void readTcp();
    void disconnect();
    void error(QAbstractSocket::SocketError err);

private:
    QTcpServer *server;
    QUdpSocket *udpSocket;
    QList<SocketContainer* > list;
    QString testMsg;
    Operation *operation;
    OperationThread *operationThread;
    SignalGun *signalGun;
};

#endif // TRANSMITTER_H
