﻿#ifndef CONFIG_H
#define CONFIG_H

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QHostAddress>

#define CONF Config::getInstance()->getConfig()
#define USERID Config::getInstance()->getConfig().userId

struct ConfigAttr {
    qint32 userId;
    QHostAddress serverIp;
    QHostAddress localIp;
    int tcpPort;
    int udpPort;
    QString localDataPath;
};


class Config {
protected:
    Config();

public:
    // 禁用拷贝构造函数
    Config(Config &other) = delete;
    // 禁用 = 运算符
    void operator=(const Config &) = delete;
    // 静态方法获取单例
    static Config *getInstance();
    // const & 修饰防止修改配置
    const ConfigAttr &getConfig();

    void setUserId(qint32 userId);

private:
    // 静态单例指针
    static Config *instance;
    ConfigAttr config;
};

#endif // SYSTEMCONFIG_H
