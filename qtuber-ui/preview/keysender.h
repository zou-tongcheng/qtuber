#ifndef KEYSENDER_H
#define KEYSENDER_H

#include <QObject>

class KeySender : public QObject{
    Q_OBJECT
public:
    KeySender();
    void setId(const qint32 &id);

public slots:
    void emitId();

signals:
    void sendId(qint32 id);

private:
    qint32 id;

};

#endif // KEYSENDER_H
