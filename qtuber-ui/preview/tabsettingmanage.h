#ifndef TABSETTINGMANAGE_H
#define TABSETTINGMANAGE_H

#include <QWidget>

#include "dialogsuccess.h"

namespace Ui {
class tabSettingManage;
}

class tabSettingManage : public QWidget
{
    Q_OBJECT

public:
    explicit tabSettingManage(QWidget *parent = nullptr);
    ~tabSettingManage();

private slots:
    void on_save_btn_clicked();

    void on_info_avator_btn_clicked();

private:
    Ui::tabSettingManage *ui;
    DialogSuccess *dialog;
};

#endif // TABSETTINGMANAGE_H
