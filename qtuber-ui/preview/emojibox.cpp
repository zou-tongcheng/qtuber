#include "emojibox.h"
#include "ui_emojibox.h"

EmojiBox::EmojiBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EmojiBox)
{
    ui->setupUi(this);
}

EmojiBox::~EmojiBox()
{
    delete ui;
}
