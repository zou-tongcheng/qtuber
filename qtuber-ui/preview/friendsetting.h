#ifndef FRIENDSETTING_H
#define FRIENDSETTING_H

#include <QWidget>

namespace Ui {
class FriendSetting;
}

class FriendSetting : public QWidget
{
    Q_OBJECT

public:
    explicit FriendSetting(QWidget *parent = nullptr);
    ~FriendSetting();

private:
    Ui::FriendSetting *ui;
};

#endif // FRIENDSETTING_H
