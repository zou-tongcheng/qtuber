QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addjsontools.cpp \
    chatitem.cpp \
    chatroom.cpp \
    config.cpp \
    dialogsuccess.cpp \
    emojipanel.cpp \
    friendsetting.cpp \
    keysender.cpp \
    linkwidget.cpp \
    main.cpp \
    mainwindow.cpp \
    mychatitem.cpp \
    mycombobox.cpp \
    recentcontactbox.cpp \
    recentcontactlabel.cpp \
    signupwidget.cpp \
    tabgroupchat.cpp \
    tabsettingmanage.cpp \
    tabsolochat.cpp \
    talkmainwindow.cpp \
    clientoperation.cpp \
    clientoperationthread.cpp \
    infohead.cpp \
    localresourcemanager.cpp \
    operation.cpp \
    operationthread.cpp \
    signalgun.cpp \
    socketcontainer.cpp \
    transmitter.cpp \
    voicemsg.cpp

HEADERS += \
    addjsontools.h \
    chatitem.h \
    chatroom.h \
    config.h \
    dialogsuccess.h \
    emojipanel.h \
    friendsetting.h \
    keysender.h \
    linkwidget.h \
    mainwindow.h \
    mychatitem.h \
    mycombobox.h \
    recentcontactbox.h \
    recentcontactlabel.h \
    signupwidget.h \
    tabgroupchat.h \
    tabsettingmanage.h \
    tabsolochat.h \
    talkmainwindow.h \
    clientoperation.h \
    clientoperationthread.h \
    infohead.h \
    localresourcemanager.h \
    operation.h \
    operationthread.h \
    signalgun.h \
    socketcontainer.h \
    transmitter.h \
    voicemsg.h


FORMS += \
    chatitem.ui \
    dialogsuccess.ui \
    emojipanel.ui \
    friendsetting.ui \
    mainwindow.ui \
    mychatitem.ui \
    signupwidget.ui \
    tabsettingmanage.ui \
    voicemsg.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += targetDISTFILES += \
    C:/Users/lenovo/Desktop/log/log/image/1.jpg \
    C:/Users/lenovo/Desktop/log/log/image/2.png \
    C:/Users/lenovo/Desktop/log/log/image/reg背景.webp \
    C:/Users/lenovo/Desktop/log/log/image/加密(1).png \
    C:/Users/lenovo/Desktop/log/log/image/团队(1).png \
    C:/Users/lenovo/Desktop/log/log/image/表情(1).png

DISTFILES += \
    btnIcon/emoji.png \
    config.json

RESOURCES += \
    btnicon.qrc \
    main.qrc
