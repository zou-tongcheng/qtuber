﻿#include "recentcontactbox.h"


RecentContactBox::RecentContactBox(
            qint32 id,
            QString nickname,
            QString time,
            QString shortMsg,
            QString avatorPath,
            qint32 type,
            QWidget *parent) : QWidget(parent) {
    // TODO: avatorPath is SB
    this->setFixedSize(280,100);
    this->setAttribute(Qt::WA_StyledBackground,true);
    this->setStyleSheet("background-color: #ffffff;"
                        "border-radius:10px;");

    RecentContactLabel *avatorLabel = new RecentContactLabel();
    // TODO: follow words is tmp

    QFile file(CONF.localDataPath + QString("Qtuber/%1").arg((int)CONF.userId) + QString("/soloavator/%1.jpg").arg(id));
    QString path;
    if(type == 0){
         path = CONF.localDataPath + QString("Qtuber/%1").arg((int)CONF.userId) + QString("/soloavator/%1.jpg").arg(id);
    }
    else if(type == 1){
         path = CONF.localDataPath + QString("Qtuber/%1").arg((int)CONF.userId) + QString("/soloavator/g%1.jpg").arg(id);
    }
    QPixmap pix;
    pix.load(path);
//    pix.load(avatorPath);
    QPixmap tempMap = pix.scaled(60,60);
    avatorLabel->setPixmap(tempMap);
    avatorLabel->setStyleSheet("border-radius:10px;");
    RecentContactLabel *nicknameLabel = new RecentContactLabel();

    QFont nameFont("Droid Serif", 10, 60);
    nicknameLabel->setText(nickname);
    nicknameLabel->setFont(nameFont);

    QFont timeFont ("Arvo", 9, 50);
    timeLabel = new RecentContactLabel();
    time = time.mid(11,5);
    qDebug() << "time is " << time;
    timeLabel->setText(time);
    timeLabel->setStyleSheet("background:transparent;");
    timeLabel->setFont(timeFont);
//Microsoft YaHei

    QFont sMsgFont ("Microsoft YaHei", 10, 60);
    shortMsgLabel = new RecentContactLabel();
    shortMsgLabel->setFont(sMsgFont);
    shortMsgLabel->setText(shortMsg);
    remind = new QLabel();
    remind->setFixedSize(10,10);
    remind->setStyleSheet("background-color:red;"
                          "border-radius:5px;");
    remind->hide();
    QHBoxLayout *msgAndRemind = new QHBoxLayout();
    msgAndRemind->addWidget(shortMsgLabel);
    msgAndRemind->addWidget(remind);
    QHBoxLayout *h1 = new QHBoxLayout();
    h1->addWidget(nicknameLabel);
    h1->addWidget(timeLabel);
    QVBoxLayout *v1 = new QVBoxLayout();
    v1->addLayout(h1);
    v1->addLayout(msgAndRemind);
//    v1->addWidget(shortMsgLabel);
    QHBoxLayout *boxLayout = new QHBoxLayout(this);
    boxLayout->addWidget(avatorLabel);
    boxLayout->addLayout(v1);
    this->setLayout(boxLayout);

    connect(avatorLabel,SIGNAL(clicked()),this,SIGNAL(clicked()));
    connect(nicknameLabel,SIGNAL(clicked()),this,SIGNAL(clicked()));
    connect(timeLabel,SIGNAL(clicked()),this,SIGNAL(clicked()));
    connect(shortMsgLabel,SIGNAL(clicked()),this,SIGNAL(clicked()));

}

RecentContactBox::~RecentContactBox(){

}

void RecentContactBox::updateBox(QString time, QString shortMsg) {
    this->timeLabel->setText(time);
    this->shortMsgLabel->setText(shortMsg);
}

void RecentContactBox::showRemind(qint32 id){
    if(this->id == id){
        this->remind->show();
    }
}

void RecentContactBox::changeTime(QString time){
    time = time.mid(11,5);
    timeLabel->setText(time);
}

void RecentContactBox::changeMsg(QString msg){
    shortMsgLabel->setText(msg);
}
