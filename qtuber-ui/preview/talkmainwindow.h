﻿#ifndef TALKMAINWINDOW_H
#define TALKMAINWINDOW_H

#include "recentcontactbox.h"
#include "linkwidget.h"
#include "tabsolochat.h"
#include "linkwidget.h"
#include "tabgroupchat.h"
#include "tabsettingmanage.h"

//JSON读写需要的头文件
#include <QJsonObject>
#include <QJsonDocument>
#include <QByteArray>
#include <QFile>
#include <QJsonArray>
#include <QDateTime>
#include <QDir>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QScrollArea>
#include "QDebug"
#include "QPushButton"
#include "QGridLayout"
#include "QToolButton"
#include "QListWidget"
#include "QLineEdit"
#include "QListWidgetItem"
#include "QToolBox"
#include "QList"
#include "QString"
#include "QTextEdit"
#include "QListWidget"
#include "QLabel"

class TalkMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    TalkMainWindow(QWidget *parent = nullptr);
    ~TalkMainWindow();

    TabSoloChat *getSolochat() const;
    TabGroupChat * getGroupchat() const;
public slots:
    void labelPressInfo();

private:
    TabSoloChat * solochat;
    TabGroupChat * groupchat;
private:
/* ????????????????????????? */
    //用户账号Id
    int accountId;
    //将聊天记录与
    //获取该用户信息
    //聊天信息 1. 联系人：利用Json存储
    //        2.

    //标签页 暂时分为 3 页

    //记录对话的账户ID
    int talkaccountId;



    QTabWidget * qtab;

    struct myLink{
        QString profile;
        QString name;
        QString time;
        QString shortInfo;
        QString peopleId;
    };

    //页面 1 负责的是关于联系人的聊天
    //页面 1 的窗口
    QWidget * qwidget1;
    //设置一个有关聊天缩略框的列表
    QList<myLink> qList;
    //缩略框的信息
    QList<QString> infomation;
    //页面 1 的滚动界面
    QScrollArea * qscrollarea1;
    //页面 1 的聊天框
    QWidget * talkWidget1;
    //输入框
    QTextEdit * le;
    //消息显示框
    QListWidget * lw;
    //保存历史消息
    QJsonArray historyArray;


    //页面 2 负责的是关于群组的聊天
    //页面 2 的窗口
    QWidget * qwidget2;
    //页面 2 的滚动界面
    QScrollArea * qscrollarea2;
    //页面 2 的聊天框
    QWidget * talkWidget2;

    //页面 3 负责的是联系人目录
    //页面 3 的窗口
    QWidget * qwidget3;
    //页面 3 的 联系分组窗口
    QToolBox * qtoolBox;

    tabSettingManage *qwidget4;

};

#endif // TALKMAINWINDOW_H
