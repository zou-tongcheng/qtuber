﻿#ifndef LINKWIDGET_H
#define LINKWIDGET_H

#include <QMainWindow>
#include <QToolBox>//工具盒类，层叠窗口，抽屉效果实现
#include <QToolButton>
#include<QVBoxLayout>
#include<QGroupBox>
#include<QDebug>
#include<QComboBox>
#include<QWidget>
#include<mycombobox.h>
#include<QPushButton>
#include<QInputDialog>
#include<QString>
#include<QJsonParseError>
#include<QJsonObject>
#include<QJsonArray>
#include<QList>

#include "friendsetting.h"
struct ToolButtonContianer {
    ToolButtonContianer(QToolButton *btn, int id) {
        this->btn = btn;
        this->id = id;
    };
    QToolButton *btn;
    int id;
};



class LinkWidget : public QMainWindow
{
    Q_OBJECT

public:
    LinkWidget(QWidget *parent = nullptr);
    ~LinkWidget();
    void initToolButton(QToolButton  **tb,QString name,QString pathpic);
    void init_friends();
    void init_group();
    void loadFile();

    typedef struct DATA //定义作图的结构体
    {
        int id;
        QString nickName;
        QString headPortrait;

    } Data;

private slots:
    void change_friends();
    void change_group();
    void click_add();
    int click_friends();
    int click_groups();


private:
    FriendSetting * tabsetting;

    QToolButton *toolbtn11;
    QToolButton *toolbtn12;
    QToolButton *toolbtn21;
    QToolButton *toolbtn22;

    QToolButton *toolbtn_top;

    QToolButton *toolbtn_creat;

    QToolButton *toolbtn_manage;


    QPushButton *add;

    QWidget *contact;
    QWidget *a;
    MyComboBox *mybox;
    QToolBox *tbox_friends;
    QToolBox *tbox_group;

    QString str;
    Data peopleData;
    Data groupData;
    QList<DATA> peopleDataList;
    QList<DATA> groupDataList;
    QToolButton *toolbtn_people;
    QToolButton *toolbtn_group;
    int idd;
    QList<ToolButtonContianer> toolButtonContianers;

    int peopleindex;
};

#endif // LINKWIDGET_H





