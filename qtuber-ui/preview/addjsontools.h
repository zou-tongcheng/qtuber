﻿#ifndef ADDJSONTOOLS_H
#define ADDJSONTOOLS_H

#include "QObject"
#include "QJsonArray"
#include "QJsonObject"
#include "QDateTime"
#include "QFile"
#include "QDebug"
#include "QJsonParseError"
#include "QListWidget"

#include "config.h"
#include "chatitem.h"
#include "mychatitem.h"
#include "voicemsg.h"
class AddJsonTools
{
public:
    AddJsonTools();
    void changeJsonFile(qint32 desId, QString type, QString data, qint32 srcId, qint32 senderId, QString dirName);
    void setChatRoomList(QListWidget* chatRoomListTemp);

private:
    QListWidget * chatRoomListTemp;

};

#endif // ADDJSONTOOLS_H
