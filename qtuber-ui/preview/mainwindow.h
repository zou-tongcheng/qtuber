﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "signupwidget.h"
#include <QMainWindow>
#include <QSystemTrayIcon>
#include "QMovie"

#include "ui_mainwindow.h"
#include "talkmainwindow.h"
#include "infohead.h"
#include "operation.h"
#include "transmitter.h"
#include "config.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void signIn();
    void reSignIn(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void toSignUp();

private:
    Ui::MainWindow *ui;
    QSystemTrayIcon * systemTray;
    QAction *minimumAct;
    QAction *maximumAct;
    QAction *restoreAct;
    QAction *quitAct;
    QMenu * pContextMenu;
};
#endif // MAINWINDOW_H
