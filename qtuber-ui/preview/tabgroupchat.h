﻿#ifndef TABGROUPCHAT_H
#define TABGROUPCHAT_H

#include <QScrollArea>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMap>

#include "recentcontactbox.h"
#include "keysender.h"
#include "chatroom.h"
#include "chatitem.h"
#include "voicemsg.h"
#include "mychatitem.h"
class TabGroupChat : public QWidget {
    Q_OBJECT
public:
    TabGroupChat();
    void init();
    void setGroupRecentContactList(QList<RecentContact> groupRecentContactList);
    void initGroupRecentContactList(QJsonDocument doc);
    void generateGroupRecentContactBoxes();
public slots:
    void handleGroupBoxClicked(qint32 id);

private:
    QScrollArea *groupScrollArea;
    QList<RecentContact> groupRecentContactList;
    QVBoxLayout * scrollLayout;
    Chatroom *chatroom;
    QWidget *scrollContainer;

    QMap<qint32, RecentContactBox*> myGroupRecentContactBoxes;
};

#endif // TABGROUPCHAT_H
