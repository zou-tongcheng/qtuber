﻿#ifndef RECENTCONTACTBOX_H
#define RECENTCONTACTBOX_H

#include "QWidget"
#include "QString"
#include "QHBoxLayout"
#include "QVBoxLayout"
#include "QObject"
#include <QDebug>

#include "recentcontactlabel.h"
#include "config.h"
#include "transmitter.h"
struct RecentContact {
    qint32 id;
    QString nickname;
    QString time;
    QString shortMsg;
    QString avatorPath;

    QString toString() {
        return QString("RecentContact{"
                       "    id = %1"
                       "    nickname = %2"
                       "    time = %3"
                       "    shortMsg = %4"
                       "    avatorPath = %5"
                       "}").arg(QString::number(id), nickname, time, shortMsg, avatorPath);
    }
};


class RecentContactBox: public QWidget{
    //一个类中使用信号和槽就要声明Q_OBJECT
    Q_OBJECT

public:
    RecentContactBox() = default;
    RecentContactBox(
            qint32 id,
            QString nickname,
            QString time,
            QString shortMsg,
            QString avatorPath,
            qint32 type,
            QWidget *parent = nullptr);
    ~RecentContactBox();

    void updateBox(QString time, QString shortMsg);
    void showRemind(qint32);
    void changeTime(QString time);
    void changeMsg(QString msg);
signals:
    void clicked();

private:
    // TODO: 如果没用就删掉
    qint32 id;
    RecentContactLabel *timeLabel;
    RecentContactLabel *shortMsgLabel;
    QLabel * remind;
};

#endif // RECENTCONTACTBOX_H
