﻿#ifndef SIGNALGUN_H
#define SIGNALGUN_H

#include "infohead.h"

#include <QHostAddress>
#include <QObject>
#include <QJsonDocument>

class SignalGun : public QObject{
    Q_OBJECT
public:
    SignalGun();

signals:
    void reSignIn(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void reSignUpAvator(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void reSignUpInfo(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void reModifyPassword(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);

    void reSendTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void reSendGroupTextMsg(InfoHead *infoHead, QJsonDocument doc, QHostAddress ip, quint16 port);
    void reSendImgMsg(InfoHead *infoHead, QByteArray bytes, QHostAddress ip, quint16 port);
    void reSendFileMsg(InfoHead *infoHead, QByteArray bytes, QHostAddress ip, quint16 port);
    void receiveMsg(qint32 remindId);

};

#endif // SIGNALGUN_H
