#include "keysender.h"

KeySender::KeySender() {

}

void KeySender::setId(const qint32 &value) {
    id = value;
}

void KeySender::emitId() {
    emit sendId(id);
}
