﻿#ifndef RECENTCONTACTLABEL_H
#define RECENTCONTACTLABEL_H

#include <QLabel>

class RecentContactLabel : public QLabel {
    Q_OBJECT
public:
    RecentContactLabel(QLabel *parent = nullptr);

protected:
    virtual void mousePressEvent(QMouseEvent *event) override;

signals:
    void clicked();

private:

};

#endif // RECENTCONTACTLABEL_H
