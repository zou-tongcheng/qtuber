#ifndef DIALOGSUCCESS_H
#define DIALOGSUCCESS_H

#include <QWidget>

namespace Ui {
class DialogSuccess;
}

class DialogSuccess : public QWidget
{
    Q_OBJECT

public:
    explicit DialogSuccess(QWidget *parent = nullptr);
    ~DialogSuccess();

private:
    Ui::DialogSuccess *ui;
};

#endif // DIALOGSUCCESS_H
