#ifndef CLIENTOPERATIONTHREAD_H
#define CLIENTOPERATIONTHREAD_H


#include <QDebug>
#include <QFile>
#include <QHostAddress>

#include "operation.h"
#include "operationthread.h"
#include "transmitter.h"

class ClientOperationThread : public OperationThread{
public:
    ClientOperationThread();
    virtual void run() override;
    virtual void setInfoHead(InfoHead *infoHead) override;
    virtual void setDataStream(QDataStream *dataStream) override;
    void setIp(const QHostAddress &ip) override;
    void setPort(const quint16 &port) override;

private:
    void defaultReturn();

private:
    InfoHead *infoHead;
    QDataStream *dataStream;
    QHostAddress ip;
    quint16 port;
};

#endif // CLIENTOPERATIONTHREAD_H
