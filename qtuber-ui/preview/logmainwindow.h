﻿#ifndef LOGMAINWINDOW_H
#define LOGMAINWINDOW_H
#include "register.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class LogMainWindow; }
QT_END_NAMESPACE

class LogMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    LogMainWindow(QWidget *parent = nullptr);
    ~LogMainWindow();


private slots:
    void on_reg_clicked();

private:
    Ui::LogMainWindow *ui;
};
#endif // LOGMAINWINDOW_H
//#endif // MAINWINDOW_H
