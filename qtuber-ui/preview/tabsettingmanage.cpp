#include "tabsettingmanage.h"
#include "ui_tabsettingmanage.h"

#include <QFileDialog>

tabSettingManage::tabSettingManage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tabSettingManage)
{
    ui->setupUi(this);
}

tabSettingManage::~tabSettingManage()
{
    delete ui;
}

void tabSettingManage::on_save_btn_clicked() {
    ui->show_nickname->setText(ui->lineEdit_nickname->text());
    dialog = new DialogSuccess();
    dialog->show();
}

void tabSettingManage::on_info_avator_btn_clicked() {
    ui->info_avator_btn->setStyleSheet(
               QString(""
                       "QPushButton {"
                       "    border: none;"
                       "    border-image: url(\"" + QFileDialog::getOpenFileName() + "\");"
                       "    border-radius: 40px;"
                       "}"
                       "QPushButton:hover {"
                       "    border-image: url(\"C:/Users/HP/Desktop/tmp/addAvator.png\");"
                       "}"
                       "QPushButton:pressed {"
                       "    border-image: url(\"C:/Users/HP/Desktop/tmp/addAvator-p.png\");"
                       "}"
                       ""));
}
/*







*/
