﻿#ifndef MYCHATITEM_H
#define MYCHATITEM_H

#include "QLabel"
#include <QWidget>

namespace Ui {
class MyChatItem;
}

class MyChatItem : public QWidget
{
    Q_OBJECT

public:
    explicit MyChatItem(QWidget *parent = nullptr);
    ~MyChatItem();
    void setAvator(QString avatorPath);
    void setTime(QString time);
    void setMsg(QString msg);
    void addPic(QString picPath);

private:
    Ui::MyChatItem *ui;
};

#endif // MYCHATITEM_H
