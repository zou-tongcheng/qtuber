#ifndef EMOJIBOX_H
#define EMOJIBOX_H

#include <QWidget>

namespace Ui {
class EmojiBox;
}

class EmojiBox : public QWidget
{
    Q_OBJECT

public:
    explicit EmojiBox(QWidget *parent = nullptr);
    ~EmojiBox();

private:
    Ui::EmojiBox *ui;
};

#endif // EMOJIBOX_H
