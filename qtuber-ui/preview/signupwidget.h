﻿#ifndef REGISTER_H
#define REGISTER_H

#include <QDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QHostAddress>
#include "infohead.h"
#include "transmitter.h"
#include "operation.h"
#include "config.h"

namespace Ui {
class SignUpWidget;
}

class SignUpWidget : public QDialog
{
    Q_OBJECT

public:
    explicit SignUpWidget(QWidget *parent = nullptr);
    ~SignUpWidget();

private slots:
    void selectAvator();
    void signUp();
    void reAvator(InfoHead *infoHead,
                              QJsonDocument doc, QHostAddress ip, quint16 port);
    void reSignUp(InfoHead *infoHead,
                              QJsonDocument doc, QHostAddress ip, quint16 port);

private:
    Ui::SignUpWidget *ui;
    QByteArray bytes;
};

#endif // REGISTER_H
